package centroenergia.cl.calculocomponentes;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Arrays;

import VisualNumerics.math.Complex;

/**
 * Simetrical Components and Park-Blundle and Clarke Phasorial Conversor
 * @author Frank Leanez
 */
public class Calculator extends AppCompatActivity {

    public static final int ABC_TO_ZERO12 = 0;
    public static final int ZERO12_TO_ABC = 1;
    public static final int ABC_TO_PARK = 2;
    public static final int PARK_TO_ABC = 3;
    public static final int ABC_TO_CLARKE = 4;
    public static final int CLARKE_TO_ABC = 5;

    /**
     * English
     */
    public static final int ENGLISH = 0;
    /**
     * Spanish
     */
    public static final int SPANISH = 1;
    /**
     * German
     */
    public static final int GERMAN = 2;

    private static final int LANGUAGES = 3;
    private static final int OPERATIONS = 6;

    private EditText MyTextField1 = null;
    private EditText MyTextField2 = null;
    private EditText MyTextField3 = null;
    private EditText MyTextField4 = null;
    private EditText MyTextField5 = null;
    private EditText MyTextField6 = null;
    private EditText MyTextField7 = null;
    private EditText MyTextField8 = null;
    private EditText MyTextField9 = null;
    private EditText MyTextField10 = null;
    private EditText MyTextField11 = null;
    private EditText MyTextField12 = null;
    private EditText MyTextField13 = null;
    private EditText MyTextField14 = null;
    private EditText MyTextField15 = null;
    private EditText MyTextField16 = null;
    private EditText MyTextField17 = null;
    private EditText MyTextField18 = null;
    private EditText MyTextField19 = null;
    private EditText MyTextField20 = null;
    private EditText MyTextField21 = null;
    private EditText MyTextField22 = null;
    private EditText MyTextField23 = null;
    private EditText MyTextField24 = null;
    private EditText MyTextField122 = null; //This is theta

    private TextView[] AllLblFields;
    private EditText[] AllTxtFields;

    private int operationMethod;
    private int language;

    private double abs_a, abs_b, arg_a, arg_b, real_a, real_b, img_a, img_b, absolute_A,argument_A,absolute_B,argument_B, p_real_A,p_img_A,p_real_B,p_img_B ;
    private final double presicion = 0.0000001;
    private double decimal;
    private Complex A =  new Complex((double)0,(double)0);
    private Complex B =  new Complex((double)0,(double)0);
    private Complex C =  new Complex((double)0,(double)0);

    private Complex A_a =  new Complex((double)0,(double)0);
    private Complex a_A =  new Complex((double)0,(double)0);
    private Complex B_b =  new Complex((double)0,(double)0);
    private Complex C_c =  new Complex((double)0,(double)0);
    //private Complex A1 =  new Complex((double)0,(double)0);
    //private Complex B1 =  new Complex((double)0,(double)0);
    //private Complex C1 =  new Complex((double)0,(double)0);
    private Complex un =  new Complex((double)0,(double)0);
    private Complex a =  new Complex((double)0,(double)0);
    private Complex aa =  new Complex((double)0,(double)0);
    private Complex fact =  new Complex((double)0,(double)0);
    private Complex A_B =  new Complex((double)0,(double)0);
    private Complex A_B_C =  new Complex((double)0,(double)0);
    private Complex a_B =  new Complex((double)0,(double)0);
    private Complex aa_C =  new Complex((double)0,(double)0);
    private Complex a_B_aa_C =  new Complex((double)0,(double)0);
    private Complex A_a_B_aa_C =  new Complex((double)0,(double)0);
    private Complex aa_B =  new Complex((double)0,(double)0);
    private Complex a_C =  new Complex((double)0,(double)0);
    private Complex aa_B_a_C =  new Complex((double)0,(double)0);
    private Complex A_aa_B_a_C =  new Complex((double)0,(double)0);

    private Complex cos1 =  new Complex((double)0,(double)0);
    private Complex cos2 =  new Complex((double)0,(double)0);
    private Complex cos3 =  new Complex((double)0,(double)0);
    private Complex cos4 =  new Complex((double)0,(double)0);
    private Complex cos5 =  new Complex((double)0,(double)0);
    private Complex cos6 =  new Complex((double)0,(double)0);

    private Complex I0 =  new Complex((double)0,(double)0);
    private Complex I1 =  new Complex((double)0,(double)0);
    private Complex I2 =  new Complex((double)0,(double)0);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculator);

        if (getIntent().hasExtra("cl.centroenergia.calculocomponentes.Method")) {
            operationMethod = getIntent().getIntExtra("cl.centroenergia.calculocomponentes.Method", ABC_TO_ZERO12);
        }
        if (getIntent().hasExtra("cl.centroenergia.calculocomponentes.Language")) {
            language = getIntent().getIntExtra("cl.centroenergia.calculocomponentes.Language", ENGLISH);
        }

        initLabels();
        initComponents();
        reset();

    }

    private void initComponents() {


        Button btnCalculate = findViewById(R.id.btnCalculate);
        btnCalculate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calculate();
            }
        });
        Button btnPlot = findViewById(R.id.btnPlot);
        btnPlot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showChart();
            }
        });
        Button btnReset = findViewById(R.id.btnReset);
        btnReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reset();
            }
        });
        Button btnHelp = findViewById(R.id.btnHelp);
        btnHelp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showHelp();
            }
        });


        //Input Text Fields:
        AllTxtFields = new EditText[25];
        MyTextField1 = findViewById(R.id.MyTextField1);
        AllTxtFields[1] = MyTextField1;
        MyTextField2 = findViewById(R.id.MyTextField2);
        AllTxtFields[2] = MyTextField2;
        MyTextField3 = findViewById(R.id.MyTextField3);
        AllTxtFields[3] = MyTextField3;
        MyTextField4 = findViewById(R.id.MyTextField4);
        AllTxtFields[4] = MyTextField4;
        MyTextField5 = findViewById(R.id.MyTextField5);
        AllTxtFields[5] = MyTextField5;
        MyTextField6 = findViewById(R.id.MyTextField6);
        AllTxtFields[6] = MyTextField6;
        MyTextField7 = findViewById(R.id.MyTextField7);
        AllTxtFields[7] = MyTextField7;
        MyTextField8 = findViewById(R.id.MyTextField8);
        AllTxtFields[8] = MyTextField8;
        MyTextField9 = findViewById(R.id.MyTextField9);
        AllTxtFields[9] = MyTextField9;
        MyTextField10 = findViewById(R.id.MyTextField10);
        AllTxtFields[10] = MyTextField10;
        MyTextField11 = findViewById(R.id.MyTextField11);
        AllTxtFields[11] = MyTextField11;
        MyTextField12 = findViewById(R.id.MyTextField12);
        AllTxtFields[12] = MyTextField12;

        //Output Text Fields:
        MyTextField13 = findViewById(R.id.MyTextField13);
        MyTextField13.setKeyListener(null); //This is in order to make it not editable
        AllTxtFields[13] = MyTextField13;
        MyTextField14 = findViewById(R.id.MyTextField14);
        MyTextField14.setKeyListener(null);
        AllTxtFields[14] = MyTextField14;
        MyTextField15 = findViewById(R.id.MyTextField15);
        MyTextField15.setKeyListener(null);
        AllTxtFields[15] = MyTextField15;
        MyTextField16 = findViewById(R.id.MyTextField16);
        MyTextField16.setKeyListener(null);
        AllTxtFields[16] = MyTextField16;
        MyTextField17 = findViewById(R.id.MyTextField17);
        MyTextField17.setKeyListener(null);
        AllTxtFields[17] = MyTextField17;
        MyTextField18 = findViewById(R.id.MyTextField18);
        MyTextField18.setKeyListener(null);
        AllTxtFields[18] = MyTextField18;
        MyTextField19 = findViewById(R.id.MyTextField19);
        MyTextField19.setKeyListener(null);
        AllTxtFields[19] = MyTextField19;
        MyTextField20 = findViewById(R.id.MyTextField20);
        MyTextField20.setKeyListener(null);
        AllTxtFields[20] = MyTextField20;
        MyTextField21 = findViewById(R.id.MyTextField21);
        MyTextField21.setKeyListener(null);
        AllTxtFields[21] = MyTextField21;
        MyTextField22 = findViewById(R.id.MyTextField22);
        MyTextField22.setKeyListener(null);
        AllTxtFields[22] = MyTextField22;
        MyTextField23 = findViewById(R.id.MyTextField23);
        MyTextField13.setKeyListener(null);
        AllTxtFields[23] = MyTextField23;
        MyTextField24 = findViewById(R.id.MyTextField24);
        MyTextField24.setKeyListener(null);
        AllTxtFields[24] = MyTextField24;

        //This is Theta:
        MyTextField122 = findViewById(R.id.MyTextField122);
        AllTxtFields[0] = MyTextField122;
        MyTextField122.setEnabled(operationMethod == ABC_TO_PARK || operationMethod == PARK_TO_ABC);

        //All Labels (Text views):
        AllLblFields = new TextView [35];
        TextView lblInput = findViewById(R.id.lblInput);
        AllLblFields[1]= lblInput;
        TextView lblOutput = findViewById(R.id.lblOutput);
        AllLblFields[26]= lblOutput;
        TextView lbl1 = findViewById(R.id.lbl1);
        AllLblFields[2]=lbl1;
        TextView lbl2 = findViewById(R.id.lbl2);
        AllLblFields[3]=lbl2;
        TextView lbl3 = findViewById(R.id.lbl3);
        AllLblFields[4]=lbl3;
        TextView lbl4 = findViewById(R.id.lbl4);
        AllLblFields[5]=lbl4;
        TextView lbl5 = findViewById(R.id.lbl5);
        AllLblFields[6]=lbl5;
        TextView lbl6 = findViewById(R.id.lbl6);
        AllLblFields[7]=lbl6;
        TextView lbl7 = findViewById(R.id.lbl7);
        AllLblFields[8]=lbl7;
        TextView lbl8 = findViewById(R.id.lbl8);
        AllLblFields[9]=lbl8;
        TextView lbl9 = findViewById(R.id.lbl9);
        AllLblFields[10]=lbl9;
        TextView lbl10 = findViewById(R.id.lbl10);
        AllLblFields[11]=lbl10;
        TextView lbl11 = findViewById(R.id.lbl11);
        AllLblFields[12]=lbl11;
        TextView lbl12 = findViewById(R.id.lbl12);
        AllLblFields[13]=lbl12;
        TextView lbl13 = findViewById(R.id.lbl13);//Theta
        AllLblFields[34]=lbl13;//Theta
        TextView lbl14 = findViewById(R.id.lbl14);
        AllLblFields[14]=lbl14;
        TextView lbl15 = findViewById(R.id.lbl15);
        AllLblFields[15]=lbl15;
        TextView lbl16 = findViewById(R.id.lbl16);
        AllLblFields[16]=lbl16;
        TextView lbl17 = findViewById(R.id.lbl17);
        AllLblFields[17]=lbl17;
        TextView lbl18 = findViewById(R.id.lbl18);
        AllLblFields[18]=lbl18;
        TextView lbl19 = findViewById(R.id.lbl19);
        AllLblFields[19]=lbl19;
        TextView lbl20 = findViewById(R.id.lbl20);
        AllLblFields[20]=lbl20;
        TextView lbl21 = findViewById(R.id.lbl21);
        AllLblFields[21]=lbl21;
        TextView lbl22 = findViewById(R.id.lbl22);
        AllLblFields[22]= lbl22;
        TextView lbl23 = findViewById(R.id.lbl23);
        AllLblFields[23]=lbl23;
        TextView lbl24 = findViewById(R.id.lbl24);
        AllLblFields[24]=lbl24;
        TextView lbl25 = findViewById(R.id.lbl25);
        AllLblFields[25]=lbl25;

        //Put text in all TextViews (according to language):
        putLabels();

        //Put the listeners:
        for (EditText e: AllTxtFields){
            if (e!=null){
                putTxtListener(e);
            }
        }
        decimal = Math.round(1/presicion);

    }

    private void calculate () {
        switch (operationMethod) {
            case ABC_TO_ZERO12:
                calculateSimetricalComponents();
                break;
            case ZERO12_TO_ABC:
                revertSimetricalComponents();
                break;
            case ABC_TO_CLARKE:
                calculateClarkeComponents();
                break;
            case CLARKE_TO_ABC:
                revertClarkeComponents();
                break;
            case ABC_TO_PARK:
                calculateParkComponents();
                break;
            case PARK_TO_ABC:
                revertParkComponents();
                break;
        }
    }

    private void calculateSimetricalComponents () {

        A_B = null;
        A_B_C = null;
        a_B = null;
        aa_C = null;
        a_B_aa_C =  null;
        A_a_B_aa_C = null;
        aa_B =  null;
        a_C =  null;
        aa_B_a_C =  null;
        A_aa_B_a_C =  null;
        I0 =  null;
        I1 =null;
        I2 = null;

        //Componente A
        double aux1=Double.parseDouble(MyTextField1.getText().toString());
        double aux2=Double.parseDouble(MyTextField4.getText().toString());
        double aux22 = aux2*Math.PI/180;

        //Componente B
        double aux3=Double.parseDouble(MyTextField2.getText().toString());
        double aux4=Double.parseDouble(MyTextField5.getText().toString());
        double aux44 = aux4*Math.PI/180;

        //Componente C
        double aux5=Double.parseDouble(MyTextField3.getText().toString());
        double aux6=Double.parseDouble(MyTextField6.getText().toString());
        double aux66 = aux6*Math.PI/180;


        // Construccion de las componentes
        A_a = null;
        A_a =  new Complex(aux1*Math.cos(aux22),aux1*Math.sin(aux22));
        B_b = null;
        B_b =  new Complex(aux3*Math.cos(aux44),aux3*Math.sin(aux44));
        C_c = null;
        C_c =  new Complex(aux5*Math.cos(aux66),aux5*Math.sin(aux66));

        //Variables a y aa
        un = null;
        un =  new Complex((double)1,(double)0);

        double a_im = (Math.sqrt(3))/2;
        a = null;
        a =  new Complex((double)(-0.5),a_im);

        aa = null;
        aa =  new Complex((double)(-0.5),-a_im);
        double uno = 1;
        double tres = 3;
        double fac = uno/tres;

        fact = null;
        fact = new Complex (fac,(double)0);


        //Creacion de las componentes

        //Cero

        A_B = Complex.add(A_a,B_b);
        A_B_C = Complex.add(A_B,C_c);
        I0 = Complex.multiply(fact,A_B_C);

        //1

        a_B = Complex.multiply(a, B_b);
        aa_C = Complex.multiply(aa,C_c);
        a_B_aa_C = Complex.add(a_B, aa_C);
        A_a_B_aa_C = Complex.add(A_a,a_B_aa_C);
        I1 = Complex.multiply(fact,A_a_B_aa_C);

        //2

        aa_B = Complex.multiply(aa, B_b);
        a_C = Complex.multiply(a,C_c);
        aa_B_a_C = Complex.add(aa_B, a_C);
        A_aa_B_a_C = Complex.add(A_a,aa_B_a_C);
        I2 = Complex.multiply(fact,A_aa_B_a_C);

        //Impresion en los texfield de los resultados de las componentes simetricas
        double aux77 = 180*(Complex.argument(I0))/Math.PI;
        double aux7 = (double)(Math.round(decimal*Complex.abs(I0)))/decimal;
        double aux8 = (double)(Math.round(decimal*aux77))/decimal;

        MyTextField13.setText(String.valueOf(aux7));
        MyTextField16.setText(String.valueOf(aux8));

        double aux99 = 180*(Complex.argument(I1))/Math.PI;
        double aux9 = (double)(Math.round(decimal*Complex.abs(I1)))/decimal;
        double aux10 = (double)(Math.round(decimal*aux99))/decimal;

        MyTextField14.setText(String.valueOf(aux9));
        MyTextField17.setText(String.valueOf(aux10));

        double aux1111 = 180*(Complex.argument(I2))/Math.PI;
        double aux11 = (double)(Math.round(decimal*Complex.abs(I2)))/decimal;
        double aux12 = (double)(Math.round(decimal*aux1111))/decimal;

        MyTextField15.setText(String.valueOf(aux11));
        MyTextField18.setText(String.valueOf(aux12));

        double aux13 =(double)(Math.round(decimal*I0.re))/decimal;
        double aux14 =(double)(Math.round(decimal*I0.im))/decimal;

        MyTextField19.setText(String.valueOf(aux13));
        MyTextField22.setText(String.valueOf(aux14));

        double aux15 =(double)(Math.round(decimal*I1.re))/decimal;
        double aux16 =(double)(Math.round(decimal*I1.im))/decimal;

        MyTextField20.setText(String.valueOf(aux15));
        MyTextField23.setText(String.valueOf(aux16));

        double aux17 =(double)(Math.round(decimal*I2.re))/decimal;
        double aux18 =(double)(Math.round(decimal*I2.im))/decimal;

        MyTextField21.setText(String.valueOf(aux17));
        MyTextField24.setText(String.valueOf(aux18));

    }

    private void revertSimetricalComponents () {
        A_B = null;
        A_B_C = null;
        a_B = null;
        aa_C = null;
        a_B_aa_C =  null;
        A_a_B_aa_C = null;
        aa_B =  null;
        a_C =  null;
        aa_B_a_C =  null;
        A_aa_B_a_C =  null;
        I0 =  null;
        I1 =null;
        I2 = null;

        //Componente A
        double aux1= Double.valueOf(MyTextField1.getText().toString());
        double aux2= Double.valueOf(MyTextField4.getText().toString());
        double aux22 = aux2*Math.PI/180;

        //Componente B
        double aux3= Double.valueOf(MyTextField2.getText().toString());
        double aux4= Double.valueOf(MyTextField5.getText().toString());
        double aux44 = aux4*Math.PI/180;

        //Componente C
        double aux5= Double.valueOf(MyTextField3.getText().toString());
        double aux6= Double.valueOf(MyTextField6.getText().toString());
        double aux66 = aux6*Math.PI/180;


        // Construccion de las componentes
        A_a = null;
        A_a =  new Complex(aux1*Math.cos(aux22),aux1*Math.sin(aux22));
        B_b = null;
        B_b =  new Complex(aux3*Math.cos(aux44),aux3*Math.sin(aux44));
        C_c = null;
        C_c =  new Complex(aux5*Math.cos(aux66),aux5*Math.sin(aux66));

        //Variables a y aa
        un = null;
        un =  new Complex((double)1,(double)0);

        double a_im = (Math.sqrt(3))/2;
        a = null;
        a =  new Complex((double)(-0.5),a_im);

        aa = null;
        aa =  new Complex((double)(-0.5),-a_im);
        double uno = 1;
        double tres = 3;
        double fac = uno/tres;

        fact = null;
        fact = new Complex (fac,(double)0);


        //Creacion de las componentes

        //Cero

        A_B = Complex.add(A_a,B_b);
        A_B_C = Complex.add(A_B,C_c);
        I0 = A_B_C;

        //1

        aa_B = Complex.multiply(aa, B_b);
        a_C = Complex.multiply(a,C_c);
        aa_B_a_C = Complex.add(aa_B, a_C);
        A_aa_B_a_C = Complex.add(A_a,aa_B_a_C);
        I1 = A_aa_B_a_C;

        //2

        a_B = Complex.multiply(a, B_b);
        aa_C = Complex.multiply(aa,C_c);
        a_B_aa_C = Complex.add(a_B, aa_C);
        A_a_B_aa_C = Complex.add(A_a,a_B_aa_C);
        I2 = A_a_B_aa_C;

        //Impresion en los texfield de los resultados de las componentes simetricas
        double aux77 = 180*(Complex.argument(I0))/Math.PI;
        double aux7 = (double)(Math.round(decimal*Complex.abs(I0)))/decimal;
        double aux8 = (double)(Math.round(decimal*aux77))/decimal;

        MyTextField13.setText(String.valueOf(aux7));
        MyTextField16.setText(String.valueOf(aux8));

        double aux99 = 180*(Complex.argument(I1))/Math.PI;
        double aux9 = (double)(Math.round(decimal*Complex.abs(I1)))/decimal;
        double aux10 = (double)(Math.round(decimal*aux99))/decimal;

        MyTextField14.setText(String.valueOf(aux9));
        MyTextField17.setText(String.valueOf(aux10));

        double aux1111 = 180*(Complex.argument(I2))/Math.PI;
        double aux11 = (double)(Math.round(decimal*Complex.abs(I2)))/decimal;
        double aux12 = (double)(Math.round(decimal*aux1111))/decimal;

        MyTextField15.setText(String.valueOf(aux11));
        MyTextField18.setText(String.valueOf(aux12));

        double aux13 =(double)(Math.round(decimal*I0.re))/decimal;
        double aux14 =(double)(Math.round(decimal*I0.im))/decimal;

        MyTextField19.setText(String.valueOf(aux13));
        MyTextField22.setText(String.valueOf(aux14));

        double aux15 =(double)(Math.round(decimal*I1.re))/decimal;
        double aux16 =(double)(Math.round(decimal*I1.im))/decimal;

        MyTextField20.setText(String.valueOf(aux15));
        MyTextField23.setText(String.valueOf(aux16));

        double aux17 =(double)(Math.round(decimal*I2.re))/decimal;
        double aux18 =(double)(Math.round(decimal*I2.im))/decimal;

        MyTextField21.setText(String.valueOf(aux17));
        MyTextField24.setText(String.valueOf(aux18));
    }

    private void calculateClarkeComponents () {
        A_B = null;
        A_B_C = null;
        a_B = null;
        aa_C = null;
        a_B_aa_C =  null;
        A_a_B_aa_C = null;
        aa_B =  null;
        a_C =  null;
        aa_B_a_C =  null;
        A_aa_B_a_C =  null;
        I0 =  null;
        I1 =null;
        I2 = null;

        //Componente A
        double aux1= Double.valueOf(MyTextField1.getText().toString());
        double aux2= Double.valueOf(MyTextField4.getText().toString());
        double aux22 = aux2*Math.PI/180;

        //Componente B
        double aux3= Double.valueOf(MyTextField2.getText().toString());
        double aux4= Double.valueOf(MyTextField5.getText().toString());
        double aux44 = aux4*Math.PI/180;

        //Componente C
        double aux5= Double.valueOf(MyTextField3.getText().toString());
        double aux6= Double.valueOf(MyTextField6.getText().toString());
        double aux66 = aux6*Math.PI/180;


        // Construccion de las componentes
        A_a = null;
        A_a =  new Complex(aux1*Math.cos(aux22),aux1*Math.sin(aux22));
        B_b = null;
        B_b =  new Complex(aux3*Math.cos(aux44),aux3*Math.sin(aux44));
        C_c = null;
        C_c =  new Complex(aux5*Math.cos(aux66),aux5*Math.sin(aux66));

        //Variables a y aa
        un = null;
        un =  new Complex((double)1,(double)0);

        double a_im = (Math.sqrt(3))/2;
        /* a = null;*/
	   /* a =  new Complex((double)(-0.5),a_im);//h

	    aa = null;
	    aa =  new Complex((double)(-0.5),-a_im); //h^2*/

        double uno = 1;
        double tres = 3;
        double fac = uno/tres;

        fact = null;
        fact = new Complex (fac,(double)0);


        //Creacion de las componentes

        //Cero
        /* SEBA!!!!!!!!!!! SEBA!!!! SEBA!!!!!!*/
        /*---------> CAMBIA ESTO!!!!!!!<-------------------*/
        A_B = Complex.add(A_a,B_b);
        A_B_C = Complex.add(A_B,C_c);
        I0 = Complex.multiply(fact,A_B_C); //1/3 * A * B * C

        //A
        a_B= Complex.multiply((double)2,A_a);
        aa_C= Complex.multiply((double)-1,B_b);
        a_B_aa_C = Complex.multiply((double)-1,C_c);
        A_B=Complex.add(a_B,aa_C);
        A_B_C= Complex.add(A_B,a_B_aa_C);
        I1=Complex.multiply(fact,A_B_C);

	/*a_B = Complex.multiply(a, B_b);  //(-1/2 + iV3/2)*B
	aa_C = Complex.multiply(aa,C_c); //(-1/2 - iV3/2)*C
	a_B_aa_C = Complex.add(a_B, aa_C);
	A_a_B_aa_C = Complex.add(A_a,a_B_aa_C);
	I1 = Complex.multiply(fact,A_a_B_aa_C);*/

        //2
        a_B= Complex.multiply((double)0,A_a);
        aa_C= Complex.multiply((double)Math.sqrt(3),B_b);
        a_B_aa_C = Complex.multiply((double)-1*Math.sqrt(3),C_c);
        A_B=Complex.add(a_B,aa_C);
        A_B_C= Complex.add(A_B,a_B_aa_C);
        I2=Complex.multiply(fact,A_B_C);


        /*---------> CAMBIA ESTO!!!!!!!<-------------------*/

        //Impresion en los texfield de los resultados de las componentes simetricas
        double aux77 = 180*(Complex.argument(I0))/Math.PI;
        double aux7 = (double)(Math.round(decimal*Complex.abs(I0)))/decimal;
        double aux8 = (double)(Math.round(decimal*aux77))/decimal;

        MyTextField13.setText(String.valueOf(aux7));
        MyTextField16.setText(String.valueOf(aux8));

        double aux99 = 180*(Complex.argument(I1))/Math.PI;
        double aux9 = (double)(Math.round(decimal*Complex.abs(I1)))/decimal;
        double aux10 = (double)(Math.round(decimal*aux99))/decimal;

        MyTextField14.setText(String.valueOf(aux9));
        MyTextField17.setText(String.valueOf(aux10));

        double aux1111 = 180*(Complex.argument(I2))/Math.PI;
        double aux11 = (double)(Math.round(decimal*Complex.abs(I2)))/decimal;
        double aux12 = (double)(Math.round(decimal*aux1111))/decimal;

        MyTextField15.setText(String.valueOf(aux11));
        MyTextField18.setText(String.valueOf(aux12));

        double aux13 =(double)(Math.round(decimal*I0.re))/decimal;
        double aux14 =(double)(Math.round(decimal*I0.im))/decimal;

        MyTextField19.setText(String.valueOf(aux13));
        MyTextField22.setText(String.valueOf(aux14));

        double aux15 =(double)(Math.round(decimal*I1.re))/decimal;
        double aux16 =(double)(Math.round(decimal*I1.im))/decimal;

        MyTextField20.setText(String.valueOf(aux15));
        MyTextField23.setText(String.valueOf(aux16));

        double aux17 =(double)(Math.round(decimal*I2.re))/decimal;
        double aux18 =(double)(Math.round(decimal*I2.im))/decimal;

        MyTextField21.setText(String.valueOf(aux17));
        MyTextField24.setText(String.valueOf(aux18));
    }

    private void revertClarkeComponents () {
        A_B = null;
        A_B_C = null;
        a_B = null;
        aa_C = null;
        a_B_aa_C =  null;
        A_a_B_aa_C = null;
        aa_B =  null;
        a_C =  null;
        aa_B_a_C =  null;
        A_aa_B_a_C =  null;
        I0 =  null;
        I1 =null;
        I2 = null;

        //Componente A
        double aux1= Double.valueOf(MyTextField1.getText().toString());
        double aux2= Double.valueOf(MyTextField4.getText().toString());
        double aux22 = aux2*Math.PI/180;

        //Componente B
        double aux3= Double.valueOf(MyTextField2.getText().toString());
        double aux4= Double.valueOf(MyTextField5.getText().toString());
        double aux44 = aux4*Math.PI/180;

        //Componente C
        double aux5= Double.valueOf(MyTextField3.getText().toString());
        double aux6= Double.valueOf(MyTextField6.getText().toString());
        double aux66 = aux6*Math.PI/180;


        // Construccion de las componentes
        A_a = null;
        A_a =  new Complex(aux1*Math.cos(aux22),aux1*Math.sin(aux22));
        B_b = null;
        B_b =  new Complex(aux3*Math.cos(aux44),aux3*Math.sin(aux44));
        C_c = null;
        C_c =  new Complex(aux5*Math.cos(aux66),aux5*Math.sin(aux66));

        //Variables a y aa
        double auxT66 = 0;
        cos1 = null;
        cos1 =  new Complex((double)Math.cos(auxT66),(double)0);
        cos2 = null;
        cos2 =  new Complex((double)Math.cos(auxT66-(120*Math.PI/180)),(double)0);
        cos3 = null;
        cos3 =  new Complex((double)Math.cos(auxT66+(120*Math.PI/180)),(double)0);

        cos4 = null;
        cos4 =  new Complex((double)-Math.sin(auxT66),(double)0);
        cos5 = null;
        cos5 =  new Complex((double)-Math.sin(auxT66-(120*Math.PI/180)),(double)0);
        cos6 = null;
        cos6 =  new Complex((double)-Math.sin(auxT66+(120*Math.PI/180)),(double)0);
        //Variables a y aa
        un = null;
        un =  new Complex((double)1,(double)0);

        double a_im = (Math.sqrt(3))/2;
        a = null;
        a =  new Complex((double)(-0.5),a_im);

        aa = null;
        aa =  new Complex((double)(-0.5),-a_im);
        double uno = 1;
        double tres = 3;
        double fac = uno/tres;

        fact = null;
        fact = new Complex (fac,(double)0);


        //Creacion de las componentes

        //Cero

        //A_B = Complex.add(A_a,B_b);
        //A_B_C = Complex.add(A_B,C_c);
        //I0 = Complex.multiply(fact,A_B_C);

        a_B = Complex.multiply(cos1, B_b);
        aa_C = Complex.multiply(cos4,C_c);
        a_B_aa_C = Complex.add(a_B, aa_C);
        A_a_B_aa_C = Complex.add(A_a,a_B_aa_C);
        I0 = A_a_B_aa_C;


        //Id
        //a_A= Complex.multiply(cos1, A_a);
        a_B = Complex.multiply(cos2, B_b);
        aa_C = Complex.multiply(cos5,C_c);
        a_B_aa_C = Complex.add(a_B, aa_C);
        A_a_B_aa_C = Complex.add(A_a,a_B_aa_C);
        I1 = A_a_B_aa_C;

        //Iq
        //a_A= Complex.multiply(cos4, A_a);
        a_B = Complex.multiply(cos3, B_b);
        aa_C = Complex.multiply(cos6,C_c);
        a_B_aa_C = Complex.add(a_B, aa_C);
        A_a_B_aa_C = Complex.add(A_a,a_B_aa_C);
        I2 = A_a_B_aa_C;

        //Impresion en los texfield de los resultados de las componentes simetricas
        double aux77 = 180*(Complex.argument(I0))/Math.PI;
        double aux7 = (double)(Math.round(decimal*Complex.abs(I0)))/decimal;
        double aux8 = (double)(Math.round(decimal*aux77))/decimal;

        MyTextField13.setText(String.valueOf(aux7));
        MyTextField16.setText(String.valueOf(aux8));

        double aux99 = 180*(Complex.argument(I1))/Math.PI;
        double aux9 = (double)(Math.round(decimal*Complex.abs(I1)))/decimal;
        double aux10 = (double)(Math.round(decimal*aux99))/decimal;

        MyTextField14.setText(String.valueOf(aux9));
        MyTextField17.setText(String.valueOf(aux10));

        double aux1111 = 180*(Complex.argument(I2))/Math.PI;
        double aux11 = (double)(Math.round(decimal*Complex.abs(I2)))/decimal;
        double aux12 = (double)(Math.round(decimal*aux1111))/decimal;

        MyTextField15.setText(String.valueOf(aux11));
        MyTextField18.setText(String.valueOf(aux12));

        double aux13 =(double)(Math.round(decimal*I0.re))/decimal;
        double aux14 =(double)(Math.round(decimal*I0.im))/decimal;

        MyTextField19.setText(String.valueOf(aux13));
        MyTextField22.setText(String.valueOf(aux14));

        double aux15 =(double)(Math.round(decimal*I1.re))/decimal;
        double aux16 =(double)(Math.round(decimal*I1.im))/decimal;

        MyTextField20.setText(String.valueOf(aux15));
        MyTextField23.setText(String.valueOf(aux16));

        double aux17 =(double)(Math.round(decimal*I2.re))/decimal;
        double aux18 =(double)(Math.round(decimal*I2.im))/decimal;

        MyTextField21.setText(String.valueOf(aux17));
        MyTextField24.setText(String.valueOf(aux18));
    }

    private void calculateParkComponents () {
        A_B = null;
        A_B_C = null;
        a_A = null;
        a_B = null;
        aa_C = null;
        a_B_aa_C =  null;
        A_a_B_aa_C = null;
        aa_B =  null;
        a_C =  null;
        aa_B_a_C =  null;
        A_aa_B_a_C =  null;
        I0 =  null;
        I1 =null;
        I2 = null;

        //Componente A
        double aux1= Double.valueOf(MyTextField1.getText().toString());
        double aux2= Double.valueOf(MyTextField4.getText().toString());
        double aux22 = aux2*Math.PI/180;

        //Componente B
        double aux3= Double.valueOf(MyTextField2.getText().toString());
        double aux4= Double.valueOf(MyTextField5.getText().toString());
        double aux44 = aux4*Math.PI/180;

        //Componente C
        double aux5= Double.valueOf(MyTextField3.getText().toString());
        double aux6= Double.valueOf(MyTextField6.getText().toString());
        double aux66 = aux6*Math.PI/180;

        //Theta
        double auxT6= Double.valueOf(MyTextField122.getText().toString());
        double auxT66 = auxT6*Math.PI/180;



        // Construccion de las componentes
        A_a = null;
        A_a =  new Complex(aux1*Math.cos(aux22),aux1*Math.sin(aux22));
        B_b = null;
        B_b =  new Complex(aux3*Math.cos(aux44),aux3*Math.sin(aux44));
        C_c = null;
        C_c =  new Complex(aux5*Math.cos(aux66),aux5*Math.sin(aux66));

        cos1 = null;
        cos1 =  new Complex((double)2*Math.cos(auxT66),(double)0);
        cos2 = null;
        cos2 =  new Complex((double)2*Math.cos(auxT66-(120*Math.PI/180)),(double)0);
        cos3 = null;
        cos3 =  new Complex((double)2*Math.cos(auxT66+(120*Math.PI/180)),(double)0);

        cos4 = null;
        cos4 =  new Complex((double)-2*Math.sin(auxT66),(double)0);
        cos5 = null;
        cos5 =  new Complex((double)-2*Math.sin(auxT66-(120*Math.PI/180)),(double)0);
        cos6 = null;
        cos6 =  new Complex((double)-2*Math.sin(auxT66+(120*Math.PI/180)),(double)0);
        //Variables a y aa
        un = null;
        un =  new Complex((double)1,(double)0);

        double a_im = (Math.sqrt(3))/2;
        a = null;
        a =  new Complex((double)(-0.5),a_im);

        aa = null;
        aa =  new Complex((double)(-0.5),-a_im);
        double uno = 1;
        double tres = 3;
        double fac = uno/tres;

        fact = null;
        fact = new Complex (fac,(double)0);


        //Creacion de las componentes

        //Cero

        A_B = Complex.add(A_a,B_b);
        A_B_C = Complex.add(A_B,C_c);
        I0 = Complex.multiply(fact,A_B_C);


        //Id
        a_A= Complex.multiply(cos1, A_a);
        a_B = Complex.multiply(cos2, B_b);
        aa_C = Complex.multiply(cos3,C_c);
        a_B_aa_C = Complex.add(a_B, aa_C);
        A_a_B_aa_C = Complex.add(a_A,a_B_aa_C);
        I1 = Complex.multiply(fact,A_a_B_aa_C);

        //Iq
        a_A= Complex.multiply(cos4, A_a);
        a_B = Complex.multiply(cos5, B_b);
        aa_C = Complex.multiply(cos6,C_c);
        a_B_aa_C = Complex.add(a_B, aa_C);
        A_a_B_aa_C = Complex.add(a_A,a_B_aa_C);
        I2 = Complex.multiply(fact,A_a_B_aa_C);


        //Impresion en los texfield de los resultados de las componentes simetricas
        double aux77 = 180*(Complex.argument(I0))/Math.PI;
        double aux7 = (double)(Math.round(decimal*Complex.abs(I0)))/decimal;
        double aux8 = (double)(Math.round(decimal*aux77))/decimal;

        MyTextField13.setText(String.valueOf(aux7));
        MyTextField16.setText(String.valueOf(aux8));

        double aux99 = 180*(Complex.argument(I1))/Math.PI;
        double aux9 = (double)(Math.round(decimal*Complex.abs(I1)))/decimal;
        double aux10 = (double)(Math.round(decimal*aux99))/decimal;

        MyTextField14.setText(String.valueOf(aux9));
        MyTextField17.setText(String.valueOf(aux10));

        double aux1111 = 180*(Complex.argument(I2))/Math.PI;
        double aux11 = (double)(Math.round(decimal*Complex.abs(I2)))/decimal;
        double aux12 = (double)(Math.round(decimal*aux1111))/decimal;

        MyTextField15.setText(String.valueOf(aux11));
        MyTextField18.setText(String.valueOf(aux12));

        double aux13 =(double)(Math.round(decimal*I0.re))/decimal;
        double aux14 =(double)(Math.round(decimal*I0.im))/decimal;

        MyTextField19.setText(String.valueOf(aux13));
        MyTextField22.setText(String.valueOf(aux14));

        double aux15 =(double)(Math.round(decimal*I1.re))/decimal;
        double aux16 =(double)(Math.round(decimal*I1.im))/decimal;

        MyTextField20.setText(String.valueOf(aux15));
        MyTextField23.setText(String.valueOf(aux16));

        double aux17 =(double)(Math.round(decimal*I2.re))/decimal;
        double aux18 =(double)(Math.round(decimal*I2.im))/decimal;

        MyTextField21.setText(String.valueOf(aux17));
        MyTextField24.setText(String.valueOf(aux18));
    }

    private void revertParkComponents () {
        A_B = null;
        A_B_C = null;
        a_A = null;
        a_B = null;
        aa_C = null;
        a_B_aa_C =  null;
        A_a_B_aa_C = null;
        aa_B =  null;
        a_C =  null;
        aa_B_a_C =  null;
        A_aa_B_a_C =  null;
        I0 =  null;
        I1 =null;
        I2 = null;

        //Componente A
        double aux1= Double.valueOf(MyTextField1.getText().toString());
        double aux2= Double.valueOf(MyTextField4.getText().toString());
        double aux22 = aux2*Math.PI/180;

        //Componente B
        double aux3= Double.valueOf(MyTextField2.getText().toString());
        double aux4= Double.valueOf(MyTextField5.getText().toString());
        double aux44 = aux4*Math.PI/180;

        //Componente C
        double aux5= Double.valueOf(MyTextField3.getText().toString());
        double aux6= Double.valueOf(MyTextField6.getText().toString());
        double aux66 = aux6*Math.PI/180;

        //Theta
        double auxT6= Double.valueOf(MyTextField122.getText().toString());
        double auxT66 = auxT6*Math.PI/180;



        // Construccion de las componentes
        A_a = null;
        A_a =  new Complex(aux1*Math.cos(aux22),aux1*Math.sin(aux22));
        B_b = null;
        B_b =  new Complex(aux3*Math.cos(aux44),aux3*Math.sin(aux44));
        C_c = null;
        C_c =  new Complex(aux5*Math.cos(aux66),aux5*Math.sin(aux66));

        cos1 = null;
        cos1 =  new Complex((double)Math.cos(auxT66),(double)0);
        cos2 = null;
        cos2 =  new Complex((double)Math.cos(auxT66-(120*Math.PI/180)),(double)0);
        cos3 = null;
        cos3 =  new Complex((double)Math.cos(auxT66+(120*Math.PI/180)),(double)0);

        cos4 = null;
        cos4 =  new Complex((double)-Math.sin(auxT66),(double)0);
        cos5 = null;
        cos5 =  new Complex((double)-Math.sin(auxT66-(120*Math.PI/180)),(double)0);
        cos6 = null;
        cos6 =  new Complex((double)-Math.sin(auxT66+(120*Math.PI/180)),(double)0);
        //Variables a y aa
        un = null;
        un =  new Complex((double)1,(double)0);

        double a_im = (Math.sqrt(3))/2;
        a = null;
        a =  new Complex((double)(-0.5),a_im);

        aa = null;
        aa =  new Complex((double)(-0.5),-a_im);
        double uno = 1;
        double tres = 3;
        double fac = uno/tres;

        fact = null;
        fact = new Complex (fac,(double)0);


        //Creacion de las componentes

        //Cero

        //A_B = Complex.add(A_a,B_b);
        //A_B_C = Complex.add(A_B,C_c);
        //I0 = Complex.multiply(fact,A_B_C);

        a_B = Complex.multiply(cos1, B_b);
        aa_C = Complex.multiply(cos4,C_c);
        a_B_aa_C = Complex.add(a_B, aa_C);
        A_a_B_aa_C = Complex.add(A_a,a_B_aa_C);
        I0 = A_a_B_aa_C;


        //Id
        //a_A= Complex.multiply(cos1, A_a);
        a_B = Complex.multiply(cos2, B_b);
        aa_C = Complex.multiply(cos5,C_c);
        a_B_aa_C = Complex.add(a_B, aa_C);
        A_a_B_aa_C = Complex.add(A_a,a_B_aa_C);
        I1 = A_a_B_aa_C;

        //Iq
        //a_A= Complex.multiply(cos4, A_a);
        a_B = Complex.multiply(cos3, B_b);
        aa_C = Complex.multiply(cos6,C_c);
        a_B_aa_C = Complex.add(a_B, aa_C);
        A_a_B_aa_C = Complex.add(A_a,a_B_aa_C);
        I2 = A_a_B_aa_C;


        //Impresion en los texfield de los resultados de las componentes simetricas
        double aux77 = 180*(Complex.argument(I0))/Math.PI;
        double aux7 = (double)(Math.round(decimal*Complex.abs(I0)))/decimal;
        double aux8 = (double)(Math.round(decimal*aux77))/decimal;

        MyTextField13.setText(String.valueOf(aux7));
        MyTextField16.setText(String.valueOf(aux8));

        double aux99 = 180*(Complex.argument(I1))/Math.PI;
        double aux9 = (double)(Math.round(decimal*Complex.abs(I1)))/decimal;
        double aux10 = (double)(Math.round(decimal*aux99))/decimal;

        MyTextField14.setText(String.valueOf(aux9));
        MyTextField17.setText(String.valueOf(aux10));

        double aux1111 = 180*(Complex.argument(I2))/Math.PI;
        double aux11 = (double)(Math.round(decimal*Complex.abs(I2)))/decimal;
        double aux12 = (double)(Math.round(decimal*aux1111))/decimal;

        MyTextField15.setText(String.valueOf(aux11));
        MyTextField18.setText(String.valueOf(aux12));

        double aux13 =(double)(Math.round(decimal*I0.re))/decimal;
        double aux14 =(double)(Math.round(decimal*I0.im))/decimal;

        MyTextField19.setText(String.valueOf(aux13));
        MyTextField22.setText(String.valueOf(aux14));

        double aux15 =(double)(Math.round(decimal*I1.re))/decimal;
        double aux16 =(double)(Math.round(decimal*I1.im))/decimal;

        MyTextField20.setText(String.valueOf(aux15));
        MyTextField23.setText(String.valueOf(aux16));

        double aux17 =(double)(Math.round(decimal*I2.re))/decimal;
        double aux18 =(double)(Math.round(decimal*I2.im))/decimal;

        MyTextField21.setText(String.valueOf(aux17));
        MyTextField24.setText(String.valueOf(aux18));
    }

    private void showChart () {

    }

    private void reset () {

        double aux1 = 0.0;
        MyTextField1.setText(String.valueOf(aux1));
        MyTextField2.setText(String.valueOf(aux1));
        MyTextField3.setText(String.valueOf(aux1));
        MyTextField4.setText(String.valueOf(aux1));
        MyTextField5.setText(String.valueOf(aux1));
        MyTextField6.setText(String.valueOf(aux1));
        MyTextField7.setText(String.valueOf(aux1));
        MyTextField8.setText(String.valueOf(aux1));
        MyTextField9.setText(String.valueOf(aux1));
        MyTextField10.setText(String.valueOf(aux1));
        MyTextField11.setText(String.valueOf(aux1));
        MyTextField12.setText(String.valueOf(aux1));
        MyTextField13.setText(String.valueOf(aux1));
        MyTextField14.setText(String.valueOf(aux1));
        MyTextField15.setText(String.valueOf(aux1));
        MyTextField16.setText(String.valueOf(aux1));
        MyTextField17.setText(String.valueOf(aux1));
        MyTextField18.setText(String.valueOf(aux1));
        MyTextField19.setText(String.valueOf(aux1));
        MyTextField20.setText(String.valueOf(aux1));
        MyTextField21.setText(String.valueOf(aux1));
        MyTextField22.setText(String.valueOf(aux1));
        MyTextField23.setText(String.valueOf(aux1));
        MyTextField24.setText(String.valueOf(aux1));
        MyTextField122.setText(String.valueOf(aux1));

    }

    private void showHelp() {
        String help = getStringLabel("help_url");
        Uri help_uri = Uri.parse(help);
        Intent gotoHelp = new Intent(Intent.ACTION_VIEW, help_uri);
        if (gotoHelp.resolveActivity(getPackageManager()) != null){
            startActivity(gotoHelp);
        }
    }

    private void updateInputValues () {

        // variables para modulo de A
        double auxA_mod_1 = Double.parseDouble(MyTextField1.getText().toString());
        double auxA_mod_2 = Complex.abs(A);
        double auxA_mod = Math.sqrt(( auxA_mod_1-auxA_mod_2)*( auxA_mod_1-auxA_mod_2));

        //variables para modulo de B
        double auxB_mod_1 = Double.parseDouble(MyTextField2.getText().toString());
        double auxB_mod_2 = Complex.abs(B);
        double auxB_mod = Math.sqrt(( auxB_mod_1-auxB_mod_2)*( auxB_mod_1-auxB_mod_2));

        //variables para modulo de C
        double auxC_mod_1 = Double.parseDouble(MyTextField3.getText().toString());
        double auxC_mod_2 = Complex.abs(C);
        double auxC_mod = Math.sqrt(( auxC_mod_1-auxC_mod_2)*( auxC_mod_1-auxC_mod_2));


        //Variables para argumento de A
        double auxA_ang_1 = Double.parseDouble(MyTextField4.getText().toString());
        double auxA_ang_2 = 180*(Complex.argument(A))/Math.PI;
        double auxA_ang = Math.sqrt(( auxA_ang_1-auxA_ang_2)*( auxA_ang_1-auxA_ang_2));

        //Variables para argumento de B
        double auxB_ang_1 = Double.parseDouble(MyTextField5.getText().toString());
        double auxB_ang_2 = 180*(Complex.argument(B))/Math.PI;
        double auxB_ang = Math.sqrt(( auxB_ang_1-auxB_ang_2)*( auxB_ang_1-auxB_ang_2));

        //Variables para argumento de C
        double auxC_ang_1 = Double.parseDouble(MyTextField6.getText().toString());
        double auxC_ang_2 = 180*(Complex.argument(C))/Math.PI;
        double auxC_ang = Math.sqrt(( auxC_ang_1-auxC_ang_2)*( auxC_ang_1-auxC_ang_2));


        //Variables para real A
        double auxA_re_1 =  Double.parseDouble(MyTextField7.getText().toString());
        double auxA_re_2 = A.re;
        double auxA_re = Math.sqrt(( auxA_re_1-auxA_re_2)*( auxA_re_1-auxA_re_2));

        //Variables para real B
        double auxB_re_1 =  Double.parseDouble(MyTextField8.getText().toString());
        double auxB_re_2 = B.re;
        double auxB_re = Math.sqrt(( auxB_re_1-auxB_re_2)*( auxB_re_1-auxB_re_2));

        //Variables para real C
        double auxC_re_1 =  Double.parseDouble(MyTextField9.getText().toString());
        double auxC_re_2 = C.re;
        double auxC_re = Math.sqrt(( auxC_re_1-auxC_re_2)*( auxC_re_1-auxC_re_2));


        //Variables para imaginario A
        double auxA_im_1 =  Double.parseDouble(MyTextField10.getText().toString());
        double auxA_im_2 = A.im;
        double auxA_im = Math.sqrt(( auxA_im_1-auxA_im_2)*( auxA_im_1-auxA_im_2));

        //Variables para imaginario B
        double auxB_im_1 =  Double.parseDouble(MyTextField11.getText().toString());
        double auxB_im_2 = B.im;
        double auxB_im = Math.sqrt(( auxB_im_1-auxB_im_2)*( auxB_im_1-auxB_im_2));

        //Variables para imaginario C
        double auxC_im_1 =  Double.parseDouble(MyTextField12.getText().toString());
        double auxC_im_2 = C.im;
        double auxC_im = Math.sqrt(( auxC_im_1-auxC_im_2)*( auxC_im_1-auxC_im_2));


        if(auxA_mod >= presicion || auxA_ang >= presicion){
            updatevaluesPolarA();
        } else if(auxB_mod >= presicion || auxB_ang >= presicion){
            updatevaluesPolarB();
        } else if(auxC_mod >= presicion || auxC_ang >= presicion){
            updatevaluesPolarC();
        } else if(auxA_re >= presicion || auxA_im >= presicion){
            updatevaluesCartesianaA();
        } else if(auxB_re >= presicion || auxB_im >= presicion){
            updatevaluesCartesianaB();
        } else if(auxC_re >= presicion || auxC_im >= presicion){
            updatevaluesCartesianaC();
        }
    }

    private void updatevaluesPolarA() {

        A = null;
        double aux1=Double.parseDouble(MyTextField1.getText().toString());
        double aux3=Double.parseDouble(MyTextField4.getText().toString());
        double aux4 = (Math.PI*aux3)/180;
        A =  new Complex(aux1*Math.cos(aux4),aux1*Math.sin(aux4));
        double aux7 = decimal*A.re;
        double aux8 = decimal*A.im;
        double aux5 = (Math.round(aux7))/decimal;
        double aux6 = (Math.round(aux8))/decimal;

        MyTextField7.setText(String.valueOf(aux5));
        MyTextField10.setText(String.valueOf(aux6));

    }

    private void updatevaluesPolarB() {

        B = null;
        double aux2=Double.parseDouble(MyTextField2.getText().toString());
        double aux4=Double.parseDouble(MyTextField5.getText().toString());
        double aux1= (Math.PI*aux4)/180;

        B =  new Complex(aux2*Math.cos(aux1),aux2*Math.sin(aux1));

        double aux7 =decimal*B.re;
        double aux8 = decimal*B.im;
        double aux5 = (Math.round(aux7))/decimal;
        double aux6 = (Math.round(aux8))/decimal;

        MyTextField8.setText(String.valueOf(aux5));
        MyTextField11.setText(String.valueOf(aux6));

    }

    private void updatevaluesPolarC() {

        C = null;
        double aux2=Double.parseDouble(MyTextField3.getText().toString());
        double aux4=Double.parseDouble(MyTextField6.getText().toString());
        double aux1= (Math.PI*aux4)/180;

        C =  new Complex(aux2*Math.cos(aux1),aux2*Math.sin(aux1));

        double aux7 =decimal*C.re;
        double aux8 = decimal*C.im;
        double aux5 = (Math.round(aux7))/decimal;
        double aux6 = (Math.round(aux8))/decimal;

        MyTextField9.setText(String.valueOf(aux5));
        MyTextField12.setText(String.valueOf(aux6));

    }

    private void updatevaluesCartesianaA() {

        A = null;
        double aux1=Double.parseDouble(MyTextField7.getText().toString());
        double aux2=Double.parseDouble(MyTextField10.getText().toString());

        A =  new Complex(aux1,aux2);
        double aux3 = 180*(Complex.argument(A))/Math.PI;
        double aux4 = (Math.round(decimal*Complex.abs(A)))/decimal;
        double aux5 = (Math.round(decimal*aux3))/decimal;

        MyTextField1.setText(String.valueOf(aux4));
        MyTextField4.setText(String.valueOf(aux5));

    }

    private void updatevaluesCartesianaB() {

        B = null;

        double aux1=Double.parseDouble(MyTextField8.getText().toString());
        double aux2=Double.parseDouble(MyTextField11.getText().toString());


        B =  new Complex(aux1,aux2);
        double aux3 = 180*(Complex.argument(B))/Math.PI;
        double aux4 = (Math.round(decimal*Complex.abs(B)))/decimal;
        double aux5 = (Math.round(decimal*aux3))/decimal;


        MyTextField2.setText(String.valueOf(aux4));
        MyTextField5.setText(String.valueOf(aux5));

    }

    private void updatevaluesCartesianaC() {

        C = null;

        double aux1=Double.parseDouble(MyTextField9.getText().toString());
        double aux2=Double.parseDouble(MyTextField12.getText().toString());


        C =  new Complex(aux1,aux2);
        double aux3 = 180*(Complex.argument(C))/Math.PI;
        double aux4 = (Math.round(decimal*Complex.abs(C)))/decimal;
        double aux5 = (Math.round(decimal*aux3))/decimal;


        MyTextField3.setText(String.valueOf(aux4));
        MyTextField6.setText(String.valueOf(aux5));

    }

    private void putTxtListener (EditText MyTextField) {
        MyTextField.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateInputValues ();
            }
        });
    }

    private void putLabels () {
        for (int i=0; i<AllLblFields.length; i++) {
            TextView t = AllLblFields[i];
            if (t!=null) {
                String s = getStringLabel(i);
                if (s!=null) {
                    t.setText(s);
                }
            }
        }
    }

    private java.util.List<String> lKeys;
    private java.util.Map[][] mLabels;
    private void initLabels () {
        String[] keys = AppLabels.getKeys();
        lKeys = Arrays.asList(keys);
        mLabels = new java.util.LinkedHashMap[LANGUAGES][OPERATIONS];
        for (int language=0; language<LANGUAGES; language++){
            for (int opera=0; opera<OPERATIONS; opera++){
                mLabels[language][opera] = new java.util.LinkedHashMap<String,String>();
                String[] labels = AppLabels.getAppLabels(opera,language);
                for (int i=0; i<keys.length; i++) {
                    mLabels[language][opera].put(keys[i],labels[i]);
                }
            }
        }
    }

    private String getStringLabel (String key) {
        return (String) mLabels[language][operationMethod].get(key);
    }

    private String getStringLabel (int id) {
        //java.util.List<String> lKeys = new java.util.LinkedList<String>(mLabels[language][operationMethod].keySet());
        String key = lKeys.get(id);
        return getStringLabel(key);
    }

}
